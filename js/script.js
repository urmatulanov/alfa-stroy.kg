window.addEventListener('scroll', () => {
    const header = document.querySelector('header')
    header.classList.toggle('sticy', window.scrollY > 0)
})

const popupImageContainer = document.querySelector('.popup__image-container')
const images = document.querySelectorAll('.image img')
const popupImage = document.querySelector('.popup__image img')
const popupClose = document.querySelector('.popup__close')

images.forEach((image) => {
    image.addEventListener('click', () => {
        popupImageContainer.style.display = 'flex'
        popupImage.src = image.getAttribute('src')
    })
})

popupClose.addEventListener('click', () => {
    popupImageContainer.style.display = 'none'
})

// мобильное меню
const nav = document.querySelector('.navbar__ul')
const navOpen = document.querySelector('.navbar__burger-icon')
const navClose = document.querySelector('.navbar__burger-icon-close')
const navLink = document.querySelectorAll('.navbar__link')
const navLinkBtn = document.querySelector('.navbar__link-button')

navOpen.addEventListener('click', () => {
    nav.classList.add('openNav')
})

navClose.addEventListener('click', () => {
    nav.classList.remove('openNav')
})

navLink.forEach(function (link) {
    link.addEventListener('click', function () {
        nav.classList.remove('openNav')
    })
})

navLinkBtn.addEventListener('click', () => {
    nav.classList.remove('openNav')
})
